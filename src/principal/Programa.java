package principal;

import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import principal.daos.PessoaDAO;
import principal.modelos.Pessoa;

public class Programa {
	

	
	
	public static void main(String[] args) {
		
		PessoaDAO pessoaDao = new PessoaDAO();
		
		
		//Criação do objeto pessoa
		Pessoa p = new Pessoa("Jão", "45454545");		
		
		//CRUD: Create
		//Integer id = pessoaDao.salvar(p);
//		System.out.println("Id: " + id);
		
		//CRUD: update
		Pessoa jao = pessoaDao.buscarPorId(5);
		jao.setNome("João Paulo"); 
		//pessoaDao.atualizar(jao);
		
		//CRUD: delete 
		//pessoaDao.excluir(4);
		
		
		//CRUD: Read
		List<Pessoa> pessoas = pessoaDao.listar();
		System.out.println();
		System.out.println("------------------");
		System.out.println("LISTA DE PESSOAS");
		for (Pessoa pessoa : pessoas) {
			System.out.println("ID: " + pessoa.getId() 
			                 + " Nome: " + pessoa.getNome());
		}
		
		
	}
	
		
}