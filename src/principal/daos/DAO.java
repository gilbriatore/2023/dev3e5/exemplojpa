package principal.daos;

import java.util.List;

public interface DAO<T> {

	List<T> listar();
	T buscarPorId(Integer id);
	void excluir(Integer id);
	void excluir(T entidade);
	Integer salvar(T entidade);
	Integer atualizar(T entidade);
	void close();
	
}
