package principal.daos;

import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import principal.modelos.Pessoa;

public class PessoaDAO implements DAO<Pessoa>{

	private EntityManagerFactory emf;
	private EntityManager em;
	
	public PessoaDAO() {
		//Configuração da persistência
		emf = Persistence.createEntityManagerFactory("ex_mysql");
		em = emf.createEntityManager();		
	}
	
	public List<Pessoa> listar(){
		
	 List<Pessoa> pessoas = em.createQuery("from Pessoa", Pessoa.class)
				.getResultList();
		return pessoas;
	}
	
	
	public Pessoa buscarPorId(Integer id) {
		Pessoa pessoa = em.find(Pessoa.class, id);
		return pessoa;
	}
	
	
	public void excluir(Integer id) {
		excluir(buscarPorId(id));
	}
	
	public void excluir(Pessoa pessoa) {
		em.getTransaction().begin();
		em.remove(pessoa);
		em.getTransaction().commit();
	}
	
	
	public Integer salvar(Pessoa pessoa) {
		//Gravação no banco de dados
		em.getTransaction().begin();
		em.persist(pessoa);
		em.getTransaction().commit();
		return pessoa.getId();
	}
	
	public Integer atualizar(Pessoa pessoa) {
		//Atualização no banco de dados
		em.getTransaction().begin();
		em.merge(pessoa);
		em.getTransaction().commit();
		return pessoa.getId();
	}
	
	public void close() {
		//Fechamento dos recursos
		em.close();
		emf.close();
	}
	
}
